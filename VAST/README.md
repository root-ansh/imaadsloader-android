# IMA Ads Loader - Android.

To use in a different app:

1. `file`>`new`> `new module` >`import aar/jar package`	> import aar file from **`VdoSDK_op`**
2. in gradle :  

```
	android {
		...
		compileOptions {
			sourceCompatibility 1.8
			targetCompatibility 1.8
		}
	}

	dependencies {
		...

		implementation(project(":adplayersdk-debug")){transitive=true}

		implementation 'com.google.ads.interactivemedia.v3:interactivemedia:3.14.0'
		implementation 'com.google.android.gms:play-services-ads-identifier:17.0.0'
		implementation 'com.google.code.gson:gson:2.8.6'
		implementation 'com.android.volley:volley:1.1.1'

	}

```

4. in activity:
```
	String url ="https://raw.githubusercontent.com/root-ansh/TestServer/master/sdk_f_response.json";
	new VdoFramework(this,url,R.string.app_name).startShowingAds();
```

