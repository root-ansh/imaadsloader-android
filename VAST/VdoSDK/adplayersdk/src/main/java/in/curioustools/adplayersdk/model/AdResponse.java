package in.curioustools.adplayersdk.model;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public  class AdResponse {

    @SerializedName("mobile")
    @Expose
    @Nullable
    private Mobile mobile;

    @SerializedName("bottomMargin")
    @Expose
    @Nullable
    private String bottomMargin;

    @SerializedName("parent_div")
    @Expose
    @Nullable
    private String parentDiv;

    @SerializedName("cancelTimeout")
    @Expose
    @Nullable
    private String cancelTimeout;

    @SerializedName("id")
    @Expose
    @Nullable
    private String id;

    @SerializedName("muted")
    @Expose
    private boolean isMuted;

    @SerializedName("playsinline")
    @Expose
    private boolean isPlaysInline;

    @SerializedName("autoplay")
    @Expose
    private boolean isAutoplay;

    @SerializedName("preload")
    @Expose
    private boolean isPreload;

    @SerializedName("old_parent_div")
    @Expose
    @Nullable
    private String oldParentDiv;

    @SerializedName("adbreak_offsets")
    @Expose
    @Nullable
    private List adbreakOffsets;

    @SerializedName("show_on")
    @Expose
    @Nullable
    private String showOn;

    @SerializedName("iframe_burst")
    @Expose
    @Nullable
    private String iframeBurst;

    @SerializedName("sources")
    @Expose
    @Nullable
    private List<Source> sources;

    @SerializedName("path")
    @Expose
    @Nullable
    private String path;


    @SerializedName("unitId")
    @Expose
    @Nullable
    private String unitId;


    @SerializedName("waterfallTags")
    @Expose
    @Nullable
    private List<WaterfallTag> waterfallTags;

    public AdResponse() {
    }

    public AdResponse(@NonNull Mobile mobile, @NonNull String bottomMargin,
                      @NonNull String parentDiv, @NonNull String cancelTimeout, @NonNull String id,
                      boolean muted, boolean playsInline, boolean autoplay, boolean preload,
                      @NonNull String oldParentDiv, @NonNull List adbreakOffsets,
                      @NonNull String showOn, @NonNull String iframeBurst, @NonNull List sources,
                      @NonNull String path, @NonNull String unitId, @NonNull List<WaterfallTag> waterfallTags) {

        super();
        this.mobile = mobile;
        this.bottomMargin = bottomMargin;
        this.parentDiv = parentDiv;
        this.cancelTimeout = cancelTimeout;
        this.id = id;
        this.isMuted = muted;
        this.isPlaysInline = playsInline;
        this.isAutoplay = autoplay;
        this.isPreload = preload;
        this.oldParentDiv = oldParentDiv;
        this.adbreakOffsets = adbreakOffsets;
        this.showOn = showOn;
        this.iframeBurst = iframeBurst;
        this.sources = sources;
        this.path = path;
        this.unitId = unitId;
        this.waterfallTags = waterfallTags;
    }



    @Nullable
    public  Mobile getMobile() {
        return this.mobile;
    }

    public  void setMobile(@Nullable Mobile var1) {
        this.mobile = var1;
    }

    @Nullable
    public  String getBottomMargin() {
        return this.bottomMargin;
    }

    public  void setBottomMargin(@Nullable String var1) {
        this.bottomMargin = var1;
    }

    @Nullable
    public  String getParentDiv() {
        return this.parentDiv;
    }

    public  void setParentDiv(@Nullable String var1) {
        this.parentDiv = var1;
    }

    @Nullable
    public  String getCancelTimeout() {
        return this.cancelTimeout;
    }

    public  void setCancelTimeout(@Nullable String var1) {
        this.cancelTimeout = var1;
    }

    @Nullable
    public  String getId() {
        return this.id;
    }

    public  void setId(@Nullable String var1) {
        this.id = var1;
    }

    public  boolean isMuted() {
        return this.isMuted;
    }

    public  void setMuted(boolean var1) {
        this.isMuted = var1;
    }

    public  boolean isPlaysInline() {
        return this.isPlaysInline;
    }

    public  void setPlaysInline(boolean var1) {
        this.isPlaysInline = var1;
    }

    public  boolean isAutoplay() {
        return this.isAutoplay;
    }

    public  void setAutoplay(boolean var1) {
        this.isAutoplay = var1;
    }

    public  boolean isPreload() {
        return this.isPreload;
    }

    public  void setPreload(boolean var1) {
        this.isPreload = var1;
    }

    @Nullable
    public  String getOldParentDiv() {
        return this.oldParentDiv;
    }

    public  void setOldParentDiv(@Nullable String var1) {
        this.oldParentDiv = var1;
    }

    @Nullable
    public  List getAdbreakOffsets() {
        return this.adbreakOffsets;
    }

    public  void setAdbreakOffsets(@Nullable List var1) {
        this.adbreakOffsets = var1;
    }

    @Nullable
    public  String getShowOn() {
        return this.showOn;
    }

    public  void setShowOn(@Nullable String var1) {
        this.showOn = var1;
    }

    @Nullable
    public  String getIframeBurst() {
        return this.iframeBurst;
    }

    public  void setIframeBurst(@Nullable String var1) {
        this.iframeBurst = var1;
    }

    @Nullable
    public  List<Source> getSources() {
        return this.sources;
    }

    public  void setSources(@Nullable List var1) {
        this.sources = var1;
    }

    @Nullable
    public  String getPath() {
        return this.path;
    }

    public  void setPath(@Nullable String var1) {
        this.path = var1;
    }

    @Nullable
    public  String getUnitId() {
        return this.unitId;
    }

    public  void setUnitId(@Nullable String var1) {
        this.unitId = var1;
    }

    @Nullable
    public  List<WaterfallTag> getWaterfallTags() {
        return this.waterfallTags;
    }

    public  void setWaterfallTags(@Nullable List var1) {
        this.waterfallTags = var1;
    }


//    @NonNull
//    public String toString() {
//        return "AdResponse{" +
//                "mobile=" + this.mobile +
//                ", bottomMargin='" + this.bottomMargin +
//                "'" + ", parentDiv='" + this.parentDiv +
//                "'" + ", cancelTimeout='" + this.cancelTimeout +
//                "'" + ", id='" + this.id +
//                "'" + ", muted=" + this.isMuted +
//                ", playsInline=" + this.isPlaysInline +
//                ", autoplay=" + this.isAutoplay +
//                ", preload=" + this.isPreload +
//                ", oldParentDiv='" + this.oldParentDiv +
//                "'" + ", adbreakOffsets=" + this.adbreakOffsets +
//                ", showOn='" + this.showOn + "'" +
//                ", iframeBurst='" + this.iframeBurst +
//                "'" + ", sources=" + this.sources +
//                ", path='" + this.path +
//                "'" + ", unitId='" + this.unitId +
//                "'" + ", waterfallTags=" + this.waterfallTags + "}";
//    }


}
