package in.curioustools.adplayersdk;

import android.content.res.Resources;
import android.util.TypedValue;

public class AdUtils {
    public static int dpToPix(int dp, Resources r) {
       //noinspection UnnecessaryLocalVariable
       int px = Math.round(TypedValue.applyDimension(
               TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));

       // TODO: 12-09-2019 create convertor;
       return px;
   }
}
