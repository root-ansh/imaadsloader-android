package in.curioustools.adplayersdk.model;

import android.view.Gravity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class LeftOrRight {

    @SerializedName("position")
    @Expose
    @Nullable
    private String position;

    @SerializedName("margin")
    @Expose
    private int margin;

    @Nullable
    public  String getPosition() {
        return this.position;
    }

    public  void setPosition(@Nullable String var1) {
        this.position = var1;
    }

    public  int getMargin() {
        return this.margin;
    }

    public  void setMargin(int var1) {
        this.margin = var1;
    }

    public int getGravityForFloatingLayout() {
        // TODO: 20-09-2019 somehow hide fldirection and add more gravity options
        if (position!=null && position.equals("left")) {
            return Gravity.START;
        }
        if (position!=null && position.equals("right")) {
            return Gravity.END;
        }
        return Gravity.CENTER;
    }

    @NonNull
    public String toString() {
        return "\n\tLeftOrRight{" +
                "\n\t\tposition='" + this.position + "'" + ",\n\t\tmargin=" + this.margin + "}";
    }

    public LeftOrRight() {
    }

    public LeftOrRight(@NonNull String position, int margin) {
        super();
        this.position = position;
        this.margin = margin;
    }
}
