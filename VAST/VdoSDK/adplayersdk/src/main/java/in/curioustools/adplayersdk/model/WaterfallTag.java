package in.curioustools.adplayersdk.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WaterfallTag {

    @SerializedName("offset")
    @Expose
    private int offset;

    @SerializedName("ad_tags")
    @Expose
    @Nullable
    private List<String> adTags;

    public  int getOffset() {
        return this.offset;
    }

    public  void setOffset(int var1) {
        this.offset = var1;
    }

    @Nullable
    public  List<String> getAdTags() {
        return this.adTags;
    }

    public  void setAdTags(@Nullable List<String> var1) {
        this.adTags = var1;
    }

//        @NonNull
//        public String toString() {
//            return "\n\tWaterfallTag{\n\t\toffset=" + this.offset + ",\n\t\tadTags=" + this.adTags + "}";
//        }

    public WaterfallTag() {
    }

    public WaterfallTag(int offset, @NonNull List adTags) {
        //Intrinsics.checkParameterIsNonNull(adTags, "adTags");
        super();
        this.offset = offset;
        this.adTags = adTags;
    }
}
