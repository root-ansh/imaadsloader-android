package in.curioustools.adplayersdk.ui_components;

import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.ads.interactivemedia.v3.api.AdDisplayContainer;
import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.AdsLoader;
import com.google.ads.interactivemedia.v3.api.AdsManager;
import com.google.ads.interactivemedia.v3.api.AdsManagerLoadedEvent;
import com.google.ads.interactivemedia.v3.api.AdsRequest;
import com.google.ads.interactivemedia.v3.api.ImaSdkFactory;
import com.google.ads.interactivemedia.v3.api.ImaSdkSettings;
import com.google.ads.interactivemedia.v3.api.player.VideoProgressUpdate;

import java.util.ArrayList;
import java.util.List;

import in.curioustools.adplayersdk.AdUtils;
import in.curioustools.adplayersdk.R;
import in.curioustools.adplayersdk.model.AdResponse;
import in.curioustools.adplayersdk.model.WaterfallTag;


@SuppressWarnings("unused")
public class AdFragment extends Fragment {
    private static final String TAG = "AdFrag>>";
    public static final String FRAGMENT_TAG = "AD_FRAGMENT";
    private AdResponse adResponse;

    //view components
    @Nullable
    private LinearLayout llVvParent;
    @Nullable
    private ImaVideoView imaVideoView;

    //-----------------------Ad related variables
    @Nullable
    private ImaSdkFactory imaSdkFactory; // created in onActivityCreated()
    @Nullable
    private AdsLoader adsLoader;// created in onActivityCreated()
    @Nullable
    private AdsManager adsManager;


    //@Nullable private AdsManager adsManager; // created in adsLoader.onAdsManagerLoaded() i.e whenever an ad is successfully loaded

    private boolean isAdsDisplayed = false;


    //constructors-------------------
    public AdFragment() {
    }

    public AdFragment(@Nullable AdResponse adResponse, int appNameRes) {
        this();
        this.adResponse = adResponse;

        Log.e(TAG, "AdFragment: constructor called with app name res id:" + appNameRes + " and adresponse=" + adResponse);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView: called");
        return inflater.inflate(R.layout.fragment_ad, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View root,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(root, savedInstanceState);
        Log.e(TAG, "onViewCreated: called");

        initUi(root);
        attachVideoCompletedListenerToVV();

        //video is tunning but ads are not running
//        extractContentUrlAndPlayContentVideo();
//        startAdsRunner();

    }
    private void initUi(View root) {
        Log.e(TAG, "initUi: called");

        llVvParent = root.findViewById(R.id.ll_video_view);
        imaVideoView = root.findViewById(R.id.ima_vv);

        //pcvMain.setVisibility(View.GONE);



        int width,height,top,bottom,gravity,margin,start,end;
        //defaults
        width = AdUtils.dpToPix(0, root.getResources());
        height = AdUtils.dpToPix(0, root.getResources());

        gravity = Gravity.END;
        margin = AdUtils.dpToPix(0,root.getResources());

        start = AdUtils.dpToPix(0,root.getResources());
        end = AdUtils.dpToPix(0,root.getResources());
        top = AdUtils.dpToPix(0, root.getResources());//always fixed
        bottom =  AdUtils.dpToPix(0, root.getResources());

        if(adResponse!=null) {
            width = AdUtils.dpToPix(adResponse.getMobile().getWidth(), root.getResources());//todo, should i handle this?
            height = AdUtils.dpToPix(adResponse.getMobile().getHeight(), root.getResources()); //todo, should i handle this?

            gravity = adResponse.getMobile().getLeftOrRight().getGravityForFloatingLayout(); //todo, should i handle this?
            margin = AdUtils.dpToPix(adResponse.getMobile().getLeftOrRight().getMargin(),root.getResources());

            if (gravity == Gravity.START) {
                start = AdUtils.dpToPix(margin, root.getResources());   //margin to start
            } else {
                end = AdUtils.dpToPix(margin, root.getResources());     //margin to end
            }
            bottom = AdUtils.dpToPix(adResponse.getMobile().getBottomMargin(), root.getResources());
        }


        Log.e(TAG, String.format(
                "initUi: final values of the player to be setted are :width,height,top,bottom,start,end,gravity,margin" +
                        "%d,%d,%d,%d,%d,%d,%d,%d", width, height,top,bottom,start,end,gravity,margin));

        FrameLayout.LayoutParams params =new FrameLayout.LayoutParams(width, height, gravity);

        params.setMargins(start,top,end,bottom);
        Log.e(TAG, "initUi: setted player params" );
        llVvParent.setLayoutParams(params);


    }
    private void attachVideoCompletedListenerToVV() {
        // Set AdLoader Listeners{
        if (imaVideoView != null){

            ImaVideoView.OnVideoCompletedListener listener= () -> {
                Log.e(TAG, "onVideoCompleted: called");
                // Handle completed event for playing post-rolls.
                if (adsLoader != null) {
                    adsLoader.contentComplete();
                } else {
                    Log.e(TAG, "attachVideoCompletedListenerToV V:adsloader is null, could perform the functions on this " );
                }
            };
            imaVideoView.addVideoCompletedListener(listener);

            Log.e(TAG, "attachVideoCompletedListenerToVV: successfully attached listener");

        }
        else {
            Log.e(TAG, "attachVideoCompletedListenerToVV: imaVideoView is null, couldn't add video completed listener" );
        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(TAG, "onActivityCreated: called" );

        initialiseSDKFactory();
        initialiseAdsLoader();// requires non null SDK Factory


        //testing here
        extractContentUrlAndPlayContentVideo();
        startAdsRunner();


    }


    private void initialiseSDKFactory() {
        // Create an imaSdkFactory.
        imaSdkFactory = ImaSdkFactory.getInstance();
        Log.e(TAG, "onActivityCreated: initialised imaSdkFactory");
    }
    private void initialiseAdsLoader() {
        Log.e(TAG, "initialiseAdsLoader: called" );
        if(imaSdkFactory!=null) {
            Log.e(TAG, "initialiseAdsLoader: ima sdk facory is not null, creating adsloader" );
            // Create an AdsLoader.
            ImaSdkSettings imaSdkSettings = imaSdkFactory.createImaSdkSettings();
            imaSdkSettings.setAutoPlayAdBreaks(false);// set to false to manually handle ads play

            AdDisplayContainer adDisplayContainer = imaSdkFactory.createAdDisplayContainer();
            adDisplayContainer.setAdContainer(llVvParent);// looks wrong since adViewGroup is null at this point

            adsLoader = imaSdkFactory.createAdsLoader(this.getContext(), imaSdkSettings, adDisplayContainer);
            Log.e(TAG, "initAdLoader: created adsLoader:" + adsLoader);

            attachAdErrorListenerToAdLoader();
            attachAdsLoadedListenerToAdLoader();

        }
        else {
            Log.e(TAG, "initialiseAdsLoader: couldn't initialise ads loader: imaSdkFactory is null. thus adsloader is also null" );
        }
    }

    private void attachAdErrorListenerToAdLoader() {
            AdErrorEvent.AdErrorListener errorListener = new AdErrorEvent.AdErrorListener() {
                @Override
                public void onAdError(AdErrorEvent adErrorEvent) {
                    Log.e(TAG, "Ad Error message: " + adErrorEvent.getError().getMessage());
                    Log.e(TAG, "Ad Error details: " + adErrorEvent.getError().toString());

                    if (imaVideoView != null) {
                        imaVideoView.play();// why are we playing when error occurs??
                        // oh alright, its an ad error. so this means we continue playing content if ad eror occurs
                    } else {
                        Log.e(TAG, "onAdError: imaVideoView is null ");
                    }
                }
            };
            if(adsLoader!=null) {
                adsLoader.addAdErrorListener(errorListener);
                Log.e(TAG, "attachAdErrorListenerToAdLoader: successfully attached error listener" );
            }
            else {
                Log.e(TAG, "attachAdErrorListenerToAdLoader: couldn't attached error listener, adloader is null" );

            }

    }
    private void attachAdsLoadedListenerToAdLoader() {
        Log.e(TAG, "attachAdsLoadedListenerToAdLoader: called" );
        AdsLoader.AdsLoadedListener adsLoadedListener = new AdsLoader.AdsLoadedListener() {
            @Override
            public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {
                Log.e(TAG, "attachAdsLoadedListenerToAdLoader>>onAdsManagerLoaded: called.");

                Log.e(TAG, "attachAdsLoadedListenerToAdLoader>>onAdsManagerLoaded: calling" +
                        " createAndStartAdManager with params: admanager loaded event:"
                        + adsManagerLoadedEvent);

                createAndStartAdManager(adsManagerLoadedEvent);
            }
        };
        Log.e(TAG, "attachAdsLoadedListenerToAdLoader: created ads loaded listener" );
        //--------------------------------------------------------------------------------------
        if(adsLoader!=null) {
            adsLoader.addAdsLoadedListener(adsLoadedListener);
            Log.e(TAG, "attachAdsLoadedListenerToAdLoader: successfully attached" +
                    " adsLoadedListener" );
        }
        else {
            Log.e(TAG, "attachAdsLoadedListenerToAdLoader: couldn't attached adsLoadedListener, adloader is null" );
        }

    }
    private void createAndStartAdManager(AdsManagerLoadedEvent adsManagerLoadedEvent) {
        Log.e(TAG, "createAndStartAdManager: called" );
        // Ads were successfully loaded, so get the AdsManager instance. AdsManager has
        // events for ad playback and errors.

        adsManager = adsManagerLoadedEvent.getAdsManager();
        Log.e(TAG, "createAndStartAdManager: adsmanager created" );

        AdErrorEvent.AdErrorListener adsManagerErrorListener = adErrorEvent -> {
            Log.e(TAG, "onAdError: Ad Error called with event: " + adErrorEvent.getError().getMessage());
            if (imaVideoView != null) {
                Log.e(TAG, "onAdError: ima video view is not null , playing video " );
                imaVideoView.play();// why are we playing when error occurs??
                // oh alright, its an ad error. so this means we continue playing content if ad eror occurs
            }
            else {
                Log.e(TAG, "onAdError: imaVideoView is null ");
            }
        };
        Log.e(TAG, "createAndStartAdManager: created aserrorlistener:"+adsManagerErrorListener );

        AdEvent.AdEventListener adsManagerEventListener = adEvent -> {
            Log.e(TAG, "AdEvent: " + adEvent.getType() + " occurred");

            if (adsManager != null) {
                switch (adEvent.getType()) {


                    case AD_BREAK_READY: {
                    // Tell the SDK to play ads as soon as we're ready. To skip this ad break,
                    // simply return from this handler without calling mAdsManager.start().
                    Log.e(TAG, "onAdEvent: calling admanager.start()");
                    //adsManager.start();
                    break;
                    }
                    case LOADED: {
                    adsManager.start();
                    break;
                    }

                    case CONTENT_PAUSE_REQUESTED: {
                    isAdsDisplayed = true;
                    if (imaVideoView != null) imaVideoView.pause();
                    break;
                    }

                    case CONTENT_RESUME_REQUESTED: {
                    isAdsDisplayed = false;
                    if (imaVideoView != null) imaVideoView.play();
                    break;
                    }

                    case ALL_ADS_COMPLETED: {
                    adsManager.destroy();
                    adsManager = null;// todo need to either make admanager global or something to make this possible
                    break;
                    }

                    default:
                    Log.e(TAG, "onAdEvent: unhandled event occurred");
                    break;
                    }
                }
                else {
                    Log.e(TAG, "onAdsManagerLoaded: asmanager is null, couldn't handle event");
                }
        };
        Log.e(TAG, "createAndStartAdManager: creates adevent listener"+adsManagerEventListener );
        // Attach event and error event listeners to recieved admanager


        if(adsManager!=null) {
            Log.e(TAG, "createAndStartAdManager: admanager is not null" );
            adsManager.addAdErrorListener(adsManagerErrorListener);
            Log.e(TAG, "createAndStartAdManager: attached an aderror listener to current admanager");

            adsManager.addAdEventListener(adsManagerEventListener);
            Log.e(TAG, "createAndStartAdManager: attached ad event listener to current admanager");

            adsManager.init();
            Log.e(TAG, "createAndStartAdManager: called adsManager.init();");

            adsManager.start();
            Log.e(TAG, "createAndStartAdManager: called adsManager.start();");
        }
        else {
            Log.e(TAG, "createAndStartAdManager: admanager is null" );
        }

    }


    private void extractContentUrlAndPlayContentVideo() {
        Log.e(TAG, "extractContentUrlAndPlayContentVideo: called");
        //extracting data from response

        String contentUrl="";
        if(adResponse!=null){
            contentUrl="https:"+adResponse.getSources().get(0).getSrc();// TODO: 30-09-2019 use all sources
            Log.e(TAG, "extractContentUrlAndPlayContentVideo: content url="+contentUrl );
        }
        else {
            Log.e(TAG, "onViewCreated: adresponse is null. thus, content url and ad tag url are null");
        }

        if(imaVideoView!=null) {
            Log.e(TAG, "extractContentUrlAndPlayContentVideo: ima video view is not null. setting video path and starting to play **CONTENT**" );
            //start content video.
            imaVideoView.setVideoPath(contentUrl);
            imaVideoView.play();
        }
        else{
            Log.e(TAG, "extractContentUrlAndPlayContentVideo: ERROR.CAN'T PLAY CONTENT!imavideoview is null" );
        }

    }

    private int timer=0;
    private void startAdsRunner() {
        Log.e(TAG, "startAdsRunner: called" );
        //todo add a method to recieve every second update and  play add for every given time interval and ad xml
        if(adResponse!=null) {
            List<WaterfallTag> tagList =adResponse.getWaterfallTags();
            tagList = tagList ==null? new ArrayList<>():tagList;

            Log.e(TAG, "startAdsRunner:waterfall taglist="+tagList );

            int maxOffset = 0;

            for (WaterfallTag tag :tagList){
                if(tag.getOffset()>maxOffset){
                    maxOffset=tag.getOffset();
                }
            }
            Log.e(TAG, "startAdsRunner: max offset" +maxOffset);

            int finalMaxOffset = maxOffset;
            Log.e(TAG, "startAdsRunner: starting ads runner thread" );
            new Thread(() -> {
                if(imaVideoView!=null) {
                    Log.e(TAG, "startAdsRunner|thread: imaVideview is not null" );
                    Log.e(TAG, "startAdsRunner|thread starting a loop from timer to max offset where timer="+timer+", max offset="+finalMaxOffset );

                    while (!imaVideoView.isPlaying()){
                        Log.e(TAG, "startAdsRunner|thread|loop0: video view is not playing content. therefore waiting for it to start in order to show ads" );
                    }
                    Log.e(TAG, "startAdsRunner: video view started playing content. start ad showing loop" );

                    while (timer<= finalMaxOffset /*&&imaVideoView.isPlaying() &&*/) {
                        Log.e(TAG, "startAdsRunner|thread|loop: timer/maxoffset = "+timer+"/"+finalMaxOffset );
                        Log.e(TAG, "startAdsRunner|thread|loop: imaVideoView.isPlaying():"+imaVideoView.isPlaying() );

                        Log.e(TAG, "startAdsRunner|thread|loop: videoPlayback :" + imaVideoView.getCurrentPosition() + "/" + imaVideoView.getDuration());
                        String adXml =getAdXMLForTime(timer);
                        Log.e(TAG, "startAdsRunner|thread|loop: adxml="+(adXml==null?"null":"{....some large adxml link...}"));
                        if(adXml != null){
                            Log.e(TAG, "startAdsRunner|thread|loop: adxml is not null" );
                            Log.e(TAG, "startAdsRunner|thread|loop:  calling showAd " );
                            showAd(adXml);
                        }
                        else{
                            Log.e(TAG, "startAdsRunner|thread|loop: adxml is null" );
                        }

                        SystemClock.sleep(2000);
                        timer++;

                    }
                    Log.e(TAG, "startAdsRunner|thread: loop finished : " );
                    Log.e(TAG, "startAdsRunner|thread: timer="+timer );
                    Log.e(TAG, "startAdsRunner|thread: imaVideoView.isPlaying()"+imaVideoView.isPlaying() );
                }
                else{
                    Log.e(TAG, "startAdsRunner|thread: imavidieoview is null" );
                }
            }).start();
        }

        else {
                Log.e(TAG, "startAdsRunner: adresponse is null. thus, ad tag url are null");
            }

    }

    @Nullable
    private String getAdXMLForTime(int timer) {
        Log.e(TAG, "getAdXMLForTime: called for timer value"+timer );
        List<WaterfallTag> tagList =adResponse.getWaterfallTags();

        tagList = tagList ==null? new ArrayList<>():tagList;

        Log.e(TAG, "startAdsRunner:waterfall taglist="+tagList );

        int maxOffset = 0;

        for (WaterfallTag tag :tagList){
            Log.e(TAG, "getAdXMLForTime: tag="+tag );
            if(tag.getOffset()==timer){
                Log.e(TAG, "getAdXMLForTime: timer value matches for some tag's offset! returning tag_0 from the list" );
                String xmlUrl =tag.getAdTags().get(0);//todo : do waterfalling
                Log.e(TAG, "getAdXMLForTime: cmlUrl="+xmlUrl );
                return xmlUrl;
            }

        }
        Log.e(TAG, "getAdXMLForTime: no tag's offset matches for timer, returning null" );
        return null;

    }

    private void showAd(@Nullable String adTagXMLUrl) {
        Log.e(TAG, "showAd: called with adTagXML="+(adTagXMLUrl==null?"null":"{some big xml link}"));

        adTagXMLUrl="Https://"+adTagXMLUrl;
        // create Request Via Factory And LoadAd
        if (imaSdkFactory != null && adsLoader != null) {
            Log.e(TAG, "showAd: imasdkfactory and adsloader are not null");

            // Create the ads request.
            AdsRequest request = imaSdkFactory.createAdsRequest();
            Log.e(TAG, "showAd: create request loader");

            request.setContentProgressProvider(() -> {
                Log.e(TAG, "showAd|getContentProgress: called");
                if (isAdsDisplayed || imaVideoView == null || imaVideoView.getDuration() <= 0) {
                    Log.e(TAG, "showAd|getContentProgress: isAdsDisplayed || svp == null || svp.getDuration() <= 0");
                    Log.e(TAG, "showAd|getContentProgress: therefore reurning VIDEO_TIME_NOT_READY");
                    return VideoProgressUpdate.VIDEO_TIME_NOT_READY;
                }
                else {
                    int position = imaVideoView.getCurrentPosition();
                    int duration = imaVideoView.getDuration();
                    VideoProgressUpdate progressUpdate = new VideoProgressUpdate(position, duration);

                    Log.e(TAG, "showAd|getContentProgress: FALSE: isAdsDisplayed || svp == null || svp.getDuration() <= 0 ");
                    Log.e(TAG, "showAd|getContentProgress: therefore returning" + progressUpdate);

                    return progressUpdate;

                }
            });

            request.setAdTagUrl(adTagXMLUrl);
            Log.e(TAG, "showAd:request.setAdTagUrl(adTagXMLUrl) ");

            // Request the ad. After the ad is loaded, onAdsManagerLoaded() will be called.
            Log.e(TAG, "showAd: calling adsLoader.requestAds(request): "+request);
            adsLoader.requestAds(request);
        }

        else {
            Log.e(TAG, "startAdsRunner: imasdkfactory or adsloader are  null");
        }


    }



    @Override
    public void onResume() {
        if (adsManager != null && isAdsDisplayed) {

            adsManager.resume();
        } else {
            imaVideoView.play();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (adsManager != null && isAdsDisplayed) {
            adsManager.pause();
        } else {
            imaVideoView.pause();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop: ");

    }

}
