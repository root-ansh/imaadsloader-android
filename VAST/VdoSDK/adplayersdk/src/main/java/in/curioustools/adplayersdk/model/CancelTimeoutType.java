package in.curioustools.adplayersdk.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class CancelTimeoutType {

    @SerializedName("type")
    @Expose
    @Nullable
    private String type;


    @SerializedName("eventtype")
    @Expose
    @Nullable
    private String eventtype;


    @SerializedName("cancelTimeout")
    @Expose
    private int cancelTimeout;

    @Nullable
    public  String getType() {
        return this.type;
    }

    public  void setType(@Nullable String var1) {
        this.type = var1;
    }

    @Nullable
    public  String getEventtype() {
        return this.eventtype;
    }

    public  void setEventtype(@Nullable String var1) {
        this.eventtype = var1;
    }

    public  int getCancelTimeout() {
        return this.cancelTimeout;
    }

    public  void setCancelTimeout(int var1) {
        this.cancelTimeout = var1;
    }

    @NonNull
    public String toString() {
        return "\n\tCancelTimeoutType{" +
                "\n\t\ttype='" + this.type + "'" + "," +
                "\n\t\teventtype='" + this.eventtype + "'" + "," +
                "\n\t\tcancelTimeout=" + this.cancelTimeout + "}";
    }

    public CancelTimeoutType() {
    }

    public CancelTimeoutType(@NonNull String type, @NonNull String eventtype, int cancelTimeout) {
        super();
        this.type = type;
        this.eventtype = eventtype;
        this.cancelTimeout = cancelTimeout;
    }
}
