package in.curioustools.adplayersdk;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Response;
import com.google.gson.Gson;

import in.curioustools.adplayersdk.model.AdResponse;
import in.curioustools.adplayersdk.network.ResponseDownloader;
import in.curioustools.adplayersdk.ui_components.AdFragment;


public class VdoFramework {

    @NonNull
    private AppCompatActivity activity;
    @NonNull
    private String api;

    private int appNameStringRes;
    private static final String TAG="VdoFramework>>";

    public VdoFramework(@NonNull AppCompatActivity activity, @NonNull String api, int appNameStringRes) {
        this.activity = activity;
        this.api = api;
        this.appNameStringRes = appNameStringRes;
    }

    public void startShowingAds() {
        new Thread(() -> {
            //synchronoous parallel thread : load response
            Response.Listener responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String responseJsonString) {
                    responseJsonString=responseJsonString==null?"":responseJsonString;

                    Log.e(TAG, "loadAdDetails:Response is: " + responseJsonString);
                    AdResponse adResponse = new Gson().fromJson(responseJsonString, AdResponse.class);
                    loadFragment(adResponse);
                }
            };
            ResponseDownloader.loadAdDetails(
                    api,
                    activity.getApplicationContext(),
                    responseListener
            );


        }).start();
    }

    private void loadFragment(@Nullable AdResponse adResponse) {
        AdFragment fragment = new AdFragment(adResponse,appNameStringRes);
        // TODO: 14-09-2019 do something about null

        //main thread load
        activity.runOnUiThread(
                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(android.R.id.content, fragment, AdFragment.FRAGMENT_TAG)
                        ::commit
        );

    }

}



// TODO: 12-09-2019 add a way to expose exoplayer's  start stop methods
