package in.curioustools.adplayersdk.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Mobile {

    @SerializedName("show")
    @Expose
    private boolean isShow;


    @SerializedName("width")
    @Expose
    private int width;

    @SerializedName("height")
    @Expose
    private int height;

    @SerializedName("bottomMargin")
    @Expose
    private int bottomMargin;

    @SerializedName("unitType")
    @Expose
    @Nullable
    private String unitType;

    @SerializedName("leftOrRight")
    @Expose
    @Nullable
    private LeftOrRight leftOrRight;

    @SerializedName("cancelTimeoutType")
    @Expose
    @Nullable
    private CancelTimeoutType cancelTimeoutType;

    @SerializedName("passback")
    @Expose
    @Nullable
    private Passback passback;

    @SerializedName("smallWidth")
    @Expose
    private int smallWidth;

    @SerializedName("smallHeight")
    @Expose
    private int smallHeight;

    public  boolean isShow() {
        return this.isShow;
    }

    public  void setShow(boolean var1) {
        this.isShow = var1;
    }

    public  int getWidth() {
        return this.width;
    }

    public  void setWidth(int var1) {
        this.width = var1;
    }

    public  int getHeight() {
        return this.height;
    }

    public  void setHeight(int var1) {
        this.height = var1;
    }

    public  int getBottomMargin() {
        return this.bottomMargin;
    }

    public  void setBottomMargin(int var1) {
        this.bottomMargin = var1;
    }

    @Nullable
    public  String getUnitType() {
        return this.unitType;
    }

    public  void setUnitType(@Nullable String var1) {
        this.unitType = var1;
    }

    @Nullable
    public  LeftOrRight getLeftOrRight() {
        return this.leftOrRight;
    }

    public  void setLeftOrRight(@Nullable LeftOrRight var1) {
        this.leftOrRight = var1;
    }

    @Nullable
    public  CancelTimeoutType getCancelTimeoutType() {
        return this.cancelTimeoutType;
    }

    public  void setCancelTimeoutType(@Nullable CancelTimeoutType var1) {
        this.cancelTimeoutType = var1;
    }

    @Nullable
    public Passback getPassback() {
        return this.passback;
    }

    public  void setPassback(@Nullable Passback var1) {
        this.passback = var1;
    }

    public  int getSmallWidth() {
        return this.smallWidth;
    }

    public  void setSmallWidth(int var1) {
        this.smallWidth = var1;
    }

    public  int getSmallHeight() {
        return this.smallHeight;
    }

    public  void setSmallHeight(int var1) {
        this.smallHeight = var1;
    }

    @NonNull
    public String toString() {
        return "\n\tMobile{" +
                "\n\t\tshow=" + this.isShow +
                "\n\t\twidth=" + this.width +
                ",\n\t\theight=" + this.height +
                ",\n\t\t bottomMargin=" + this.bottomMargin +
                ", \n\t\tunitType='" + this.unitType + "'" + "," +
                "\n\t\t leftOrRight=" + this.leftOrRight + "," +
                "\n\t\tcancelTimeoutType=" + this.cancelTimeoutType +
                ",\n\t\t passback=" + this.passback +
                ",\n\t\t smallWidth=" + this.smallWidth +
                ",\n\t\t smallHeight=" + this.smallHeight + "}";
    }

    public Mobile() {
    }

    public Mobile(boolean show, int width, int height, int bottomMargin, @NonNull String unitType, @NonNull LeftOrRight leftOrRight, @NonNull CancelTimeoutType cancelTimeoutType, @NonNull Passback passback, int smallWidth, int smallHeight) {
        //Intrinsics.checkParameterIsNonNull(unitType, "unitType");
        //Intrinsics.checkParameterIsNonNull(leftOrRight, "leftOrRight");
        //Intrinsics.checkParameterIsNonNull(cancelTimeoutType, "cancelTimeoutType");
        //Intrinsics.checkParameterIsNonNull(passback, "passback");
        super();
        this.isShow = show;
        this.width = width;
        this.height = height;
        this.bottomMargin = bottomMargin;
        this.unitType = unitType;
        this.leftOrRight = leftOrRight;
        this.cancelTimeoutType = cancelTimeoutType;
        this.passback = passback;
        this.smallWidth = smallWidth;
        this.smallHeight = smallHeight;
    }
}
