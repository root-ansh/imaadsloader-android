package in.curioustools.vdosdktester;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import in.curioustools.adplayersdk.VdoFramework;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String url ="https://raw.githubusercontent.com/root-ansh/TestServer/master/sdk_f_response.json";
        new VdoFramework(this,url,R.string.app_name).startShowingAds();
    }
}
