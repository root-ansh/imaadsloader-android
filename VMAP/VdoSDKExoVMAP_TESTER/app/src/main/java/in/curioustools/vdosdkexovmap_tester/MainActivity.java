package in.curioustools.vdosdkexovmap_tester;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import in.curioustools.adplayersdk.sdk.VdoFramework;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MAIN_ACT>>>";

    VdoFramework framework;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate: called");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFramework();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: called");
        super.onResume();
        if (framework == null) {
            initFramework();
        }
        framework.startShowingAds();


    }

    private void initFramework() {
        framework = new VdoFramework(
                this,
                "https://raw.githubusercontent.com/root-ansh/TestServer/master/responsee_f_vmap.json");

    }


    @Override
    protected void onPause() {
        Log.e(TAG, "onPause: called");
        super.onPause();

        framework.stopShowingAds();
    }
}

