package in.curioustools.vdosdkexovmap.sdk;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.android.volley.Response;

import com.google.gson.Gson;

import in.curioustools.vdosdkexovmap.sdk.model.AdResponse;
import in.curioustools.vdosdkexovmap.sdk.network.ResponseDownloader;
import in.curioustools.vdosdkexovmap.sdk.ui_components.AdFragment;

public class VdoFramework {

    @NonNull
    private AppCompatActivity activity;
    @NonNull
    private String api;

    private static final String TAG = "VdoFramework>>";

    public VdoFramework(@NonNull AppCompatActivity activity, @NonNull String api) {
        this.activity = activity;
        this.api = api;
    }

    public void startShowingAds() {
        new Thread(() -> {
            //synchronous parallel thread : load response
            Response.Listener<String> responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String responseJsonString) {
                    responseJsonString = responseJsonString == null ? "" : responseJsonString;

                    Log.e(TAG, "loadAdDetails:Response is: " + responseJsonString);
                    AdResponse adResponse = new Gson().fromJson(responseJsonString, AdResponse.class);
                    loadFragment(adResponse);
                }
            };
            ResponseDownloader.loadAdDetails(api, activity.getApplicationContext(), responseListener);
        }).start();
    }

    private void loadFragment(@Nullable AdResponse adResponse) {

        if (adResponse == null) {
            Log.e(TAG, "loadFragment: adResponse==null, therefre not adding a fragment at all");
        }
        else {

            Log.e(TAG, "loadFragment: adResponse!=null, thus adding fragment to screen");

            AdFragment fragment = new AdFragment(adResponse);
            //main thread load
            activity.runOnUiThread(
                    activity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(android.R.id.content, fragment, AdFragment.FRAGMENT_TAG)
                            ::commit
            );

        }
    }

    public  void stopShowingAds(){
        FragmentManager manager =activity.getSupportFragmentManager();

        while (manager.getBackStackEntryCount()!=0){
            manager.popBackStack();
        }

    }

}

// TODO: 12-09-2019 add a way to expose exoplayer's  start stop methods
