package in.curioustools.adplayersdk.sdk.ui_components;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;

import in.curioustools.adplayersdk.R;
import in.curioustools.adplayersdk.sdk.AdUtils;
import in.curioustools.adplayersdk.sdk.MyTags;
import in.curioustools.adplayersdk.sdk.model.AdResponse;



/*
 * A Fragment that is added to paren't ui only nd only if
 * */
public class AdFragment extends Fragment {

    //----vars--extra--------
    public static final String FRAGMENT_TAG = "AD_FRAGMENT";
    private static final String TAG = "AdFRAG>>";


    //-----ui-------------
    @Nullable
    private PlayerView pvMain;

    //-----ad-----------

    @Nullable
    private AdResponse adResponse;  // adresponse is almost never null except when the system automatically restores the fragment


    @Nullable
    private ImaAdsLoader imaAdsLoader;


    @Nullable
    SimpleExoPlayer absPlayerInternal;


    //constructors------
    public AdFragment() {
        Log.e(TAG, "AdFragment: constructor called. initialised adResponse with empty object");
        this.adResponse = new AdResponse();
    }


    public AdFragment(@NonNull AdResponse adResponse) {
        //if response comes via VdoFramework.class, its instance and its internal objects would all be non null
        Log.e(TAG, "AdFragment: constructor called. adresponse=" + adResponse);
        this.adResponse = adResponse;

    }
    //--------

    @Override
    public void onAttach(@NonNull Context context) {
        Log.e(TAG, "onAttach: called");
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ad, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View root, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onViewCreated: called");
        super.onViewCreated(root, savedInstanceState);

        initUi(root);

    }

    private void initUi(View root) {
        Log.e(TAG, "initUi: called");


        if (adResponse == null) {
            Log.e(TAG, "initUi: adresponse is null. therefore not initialising ui and " +
                    "exoplayer internals at all");
        } else {

            this.pvMain = root.findViewById(R.id.pv_main);

            pvMain.setUseController(false);

            View videoContainer = root.findViewById(R.id.ll_video_view);


            int width, height, top, bottom, gravity, margin, start, end;

            width = AdUtils.dpToPix(adResponse.getMobile().getWidth(), root.getResources());//todo, should i handle this?
            height = AdUtils.dpToPix(adResponse.getMobile().getHeight(), root.getResources()); //todo, should i handle this?
            gravity = adResponse.getMobile().getLeftOrRight().getGravityForFloatingLayout(); //todo, should i handle this?
            margin = AdUtils.dpToPix(adResponse.getMobile().getLeftOrRight().getMargin(), root.getResources());

            top = AdUtils.dpToPix(0, root.getResources());
            end = AdUtils.dpToPix(0, root.getResources());
            start = AdUtils.dpToPix(0, root.getResources());

            if (gravity == Gravity.START) {
                start = AdUtils.dpToPix(margin, root.getResources());   //margin to start
            } else {
                end = AdUtils.dpToPix(margin, root.getResources());     //margin to end
            }
            bottom = AdUtils.dpToPix(adResponse.getMobile().getBottomMargin(), root.getResources());


            Log.e(TAG, String.format(
                    "initUi: final values of the player to be setted are :width,height,top,bottom,start,end,gravity,margin" +
                            "%d,%d,%d,%d,%d,%d,%d,%d", width, height, top, bottom, start, end, gravity, margin));

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width, height, gravity);

            params.setMargins(start, top, end, bottom);

            Log.e(TAG, "initUi: setted player params");
            videoContainer.setLayoutParams(params);
        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onActivityCreated: called");
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onStart() {
        Log.e(TAG, "onStart: called");
        super.onStart();


        startShowingAds();
    }


    private void startShowingAds() {
        if (pvMain == null) {
            Log.e(TAG, "startShowingAds: can't show since ui is null");
            return;
        }
        if (adResponse == null) {
            Log.e(TAG, "startShowingAds: can't show since adresponse is null. ");
            return;
        }

        Log.e(TAG, "startShowingAds: called");
        Context ctx = pvMain.getContext();

        String waterfallTagVMAP = adResponse.getWaterFallVMAP(); //todo: correct it

//        String waterfallTagVMAP = MyTags.AD_URL_VMAP_G5;


        Log.e(TAG, "startShowingAds: final waterfall tag=" + waterfallTagVMAP);

        Uri vmapAdUri = Uri.parse(waterfallTagVMAP);
        Log.e(TAG, "startShowingAds: final adUri" + vmapAdUri);


        imaAdsLoader = new ImaAdsLoader.Builder(ctx)
                .setMediaLoadTimeoutMs(60000)
                .setVastLoadTimeoutMs(60000)
                .buildForAdTag(vmapAdUri);
        Log.e(TAG, "startShowingAds: imaAdsloaderObj=" + imaAdsLoader);


        TrackSelector trackSelectorDef = new DefaultTrackSelector();
        absPlayerInternal = ExoPlayerFactory.newSimpleInstance(ctx, trackSelectorDef);

        absPlayerInternal.addAnalyticsListener(new EventLogger((MappingTrackSelector) trackSelectorDef));

        pvMain.setPlayer(absPlayerInternal);

        String userAgent = Util.getUserAgent(ctx, FRAGMENT_TAG);// TODO: 23/10/19 is it correct? i am not passing app name
        DefaultDataSourceFactory defDataSourceFactory = new DefaultDataSourceFactory(ctx, userAgent);

        String contentUrl = adResponse.getSources().get(0).getSrc();//todo: should i handle this?
        contentUrl = "https:" + contentUrl;

        Uri uriOfContentUrl = Uri.parse(contentUrl);
        Log.e(TAG, "addMediaSource: contentUri:" + contentUrl);
        Log.e(TAG, "addMediaSource: uriContenturl:" + uriOfContentUrl);

        MediaSource mediaSource = new ProgressiveMediaSource
                .Factory(defDataSourceFactory)
                .createMediaSource(uriOfContentUrl);

        AdsLoader.AdViewProvider adViewProvider = new AdsLoader.AdViewProvider() {
            public ViewGroup getAdViewGroup() {
                return pvMain.getOverlayFrameLayout();
            }

            public View[] getAdOverlayViews() {
                return new View[0];
            }
        };

        AdsMediaSource adsMediaSource = new AdsMediaSource(
                mediaSource, defDataSourceFactory,
                imaAdsLoader, adViewProvider);

        absPlayerInternal.prepare(adsMediaSource);
        imaAdsLoader.setPlayer(absPlayerInternal);

        //absPlayerInternal.setVolume(0f);// todo : >< for setting volume
        absPlayerInternal.setPlayWhenReady(true);// todo : >< for autoplay
        pvMain.setUseController(false);//todo >< for showing/hiding internal media controller

    }


    @Override
    public void onResume() {
        Log.e(TAG, "onResume: called");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause: called");
        super.onPause();
        stopPlayerAndAds();

    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop: called");
        super.onStop();
        startShowingAds();

    }


    private void stopPlayerAndAds() {
        Log.e(TAG, "stopPlayerAndAds: called");

        if (absPlayerInternal != null) {
            absPlayerInternal.release();
        }
        if (imaAdsLoader != null) {
            imaAdsLoader.release();
        }
    }


}


