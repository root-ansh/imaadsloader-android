package in.curioustools.adplayersdk.sdk.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Source {

    @SerializedName("src")
    @Expose
    @Nullable
    private String src;

    @SerializedName("type")
    @Expose
    @Nullable
    private String type;

    @Nullable
    public  String getSrc() {
        return this.src;
    }

    public  void setSrc(@Nullable String var1) {
        this.src = var1;
    }

    @Nullable
    public  String getType() {
        return this.type;
    }

    public  void setType(@Nullable String var1) {
        this.type = var1;
    }

    @NonNull
    public String toString() {
        return "\n\tSource{" +
                "\n\t\tsrc='" + this.src + "'" +
                "\n\t\t, type='" + this.type + "'" + "}";
    }

    public Source() {
    }

    public Source(@NonNull String src, @NonNull String type) {
        //Intrinsics.checkParameterIsNonNull(src, "src");
        //Intrinsics.checkParameterIsNonNull(type, "type");
        super();
        this.src = src;
        this.type = type;
    }
}
