package in.curioustools.adplayersdk.sdk.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Passback {

    @SerializedName("allow")
    @Expose
    private boolean isAllow;

    @SerializedName("src")
    @Expose
    @Nullable
    private String src;

    @SerializedName("string")
    @Expose
    @Nullable
    private String string;

    @SerializedName("timeout")
    @Expose
    private int timeout;

    public  boolean isAllow() {
        return this.isAllow;
    }

    public  void setAllow(boolean var1) {
        this.isAllow = var1;
    }

    @Nullable
    public  String getSrc() {
        return this.src;
    }

    public  void setSrc(@Nullable String var1) {
        this.src = var1;
    }

    @Nullable
    public  String getString() {
        return this.string;
    }

    public  void setString(@Nullable String var1) {
        this.string = var1;
    }

    public  int getTimeout() {
        return this.timeout;
    }

    public  void setTimeout(int var1) {
        this.timeout = var1;
    }

    @NonNull
    public String toString() {
        return "\n\tPassback{" +
                "\n\t\tallow=" + this.isAllow +
                ",\n\t\tsrc='" + this.src + "'" +
                ",\n\t\tstring='" + this.string + "'" +
                ",\n\t\ttimeout=" + this.timeout + "}";
    }

    public Passback() {
    }

    public Passback(boolean allow, @NonNull String src, @NonNull String string, int timeout) {

        super();
        this.isAllow = allow;
        this.src = src;
        this.string = string;
        this.timeout = timeout;
    }
}
