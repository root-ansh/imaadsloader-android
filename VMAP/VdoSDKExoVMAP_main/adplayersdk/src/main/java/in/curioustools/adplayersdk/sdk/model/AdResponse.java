package in.curioustools.adplayersdk.sdk.model;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public  class AdResponse {

    @SerializedName("mobile")
    @Expose
    @Nullable
    private Mobile mobile;

    @SerializedName("bottomMargin")
    @Expose
    @Nullable
    private String bottomMargin;

    @SerializedName("parent_div")
    @Expose
    @Nullable
    private String parentDiv;

    @SerializedName("cancelTimeout")
    @Expose
    @Nullable
    private String cancelTimeout;

    @SerializedName("id")
    @Expose
    @Nullable
    private String id;

    @SerializedName("muted")
    @Expose
    private boolean isMuted;

    @SerializedName("playsinline")
    @Expose
    private boolean isPlaysInline;

    @SerializedName("autoplay")
    @Expose
    private boolean isAutoplay;

    @SerializedName("preload")
    @Expose
    private boolean isPreload;

    @SerializedName("old_parent_div")
    @Expose
    @Nullable
    private String oldParentDiv;

    @SerializedName("adbreak_offsets")
    @Expose
    @Nullable
    private List adbreakOffsets;

    @SerializedName("show_on")
    @Expose
    @Nullable
    private String showOn;

    @SerializedName("iframe_burst")
    @Expose
    @Nullable
    private String iframeBurst;

    @SerializedName("sources")
    @Expose
    @Nullable
    private List<Source> sources;

    @SerializedName("path")
    @Expose
    @Nullable
    private String path;


    @SerializedName("unitId")
    @Expose
    @Nullable
    private String unitId;



    @SerializedName("waterfall_vmap")
    @Expose
    @Nullable
    private String waterFallVMAP;


    @SerializedName("waterfallTags")
    @Expose
    @Nullable
    private List<WaterfallTag> waterfallTags;

    public AdResponse() {
    }

    public AdResponse(@NonNull Mobile mobile, @NonNull String bottomMargin,
                      @NonNull String parentDiv, @NonNull String cancelTimeout, @NonNull String id,
                      boolean isMuted, boolean isPlaysInline, boolean isAutoplay, boolean isPreload,
                      @NonNull String oldParentDiv, @NonNull List adbreakOffsets,
                      @NonNull String showOn, @NonNull String iframeBurst, @NonNull List<Source> sources,
                      @Nullable String path, @NonNull String unitId, @NonNull String waterFallVMAP,
                      @NonNull List<WaterfallTag> waterfallTags) {
        this.mobile = mobile;
        this.bottomMargin = bottomMargin;
        this.parentDiv = parentDiv;
        this.cancelTimeout = cancelTimeout;
        this.id = id;
        this.isMuted = isMuted;
        this.isPlaysInline = isPlaysInline;
        this.isAutoplay = isAutoplay;
        this.isPreload = isPreload;
        this.oldParentDiv = oldParentDiv;
        this.adbreakOffsets = adbreakOffsets;
        this.showOn = showOn;
        this.iframeBurst = iframeBurst;
        this.sources = sources;
        this.path = path;
        this.unitId = unitId;
        this.waterFallVMAP = waterFallVMAP;
        this.waterfallTags = waterfallTags;
    }

    @Nullable
    public  Mobile getMobile() {
        return this.mobile;
    }

    public  void setMobile(@Nullable Mobile var1) {
        this.mobile = var1;
    }

    @Nullable
    public  String getBottomMargin() {
        return this.bottomMargin;
    }

    public  void setBottomMargin(@Nullable String var1) {
        this.bottomMargin = var1;
    }

    @Nullable
    public  String getParentDiv() {
        return this.parentDiv;
    }

    public  void setParentDiv(@Nullable String var1) {
        this.parentDiv = var1;
    }

    @Nullable
    public  String getCancelTimeout() {
        return this.cancelTimeout;
    }

    public  void setCancelTimeout(@Nullable String var1) {
        this.cancelTimeout = var1;
    }

    @Nullable
    public  String getId() {
        return this.id;
    }

    public  void setId(@Nullable String var1) {
        this.id = var1;
    }

    public  boolean isMuted() {
        return this.isMuted;
    }

    public  void setMuted(boolean var1) {
        this.isMuted = var1;
    }

    public  boolean isPlaysInline() {
        return this.isPlaysInline;
    }

    public  void setPlaysInline(boolean var1) {
        this.isPlaysInline = var1;
    }

    public  boolean isAutoplay() {
        return this.isAutoplay;
    }

    public  void setAutoplay(boolean var1) {
        this.isAutoplay = var1;
    }

    public  boolean isPreload() {
        return this.isPreload;
    }

    public  void setPreload(boolean var1) {
        this.isPreload = var1;
    }

    @Nullable
    public  String getOldParentDiv() {
        return this.oldParentDiv;
    }

    public  void setOldParentDiv(@Nullable String var1) {
        this.oldParentDiv = var1;
    }

    @Nullable
    public  List getAdbreakOffsets() {
        return this.adbreakOffsets;
    }

    public  void setAdbreakOffsets(@Nullable List var1) {
        this.adbreakOffsets = var1;
    }

    @Nullable
    public  String getShowOn() {
        return this.showOn;
    }

    public  void setShowOn(@Nullable String var1) {
        this.showOn = var1;
    }

    @Nullable
    public  String getIframeBurst() {
        return this.iframeBurst;
    }

    public  void setIframeBurst(@Nullable String iframeBurst) {
        this.iframeBurst = iframeBurst;
    }

    @Nullable
    public  List<Source> getSources() {
        return this.sources;
    }

    public void setSources(@Nullable List<Source> sources) {
        this.sources = sources;
    }


    @Nullable
    public  String getPath() {
        return this.path;
    }

    public  void setPath(@Nullable String var1) {
        this.path = var1;
    }

    @Nullable
    public  String getUnitId() {
        return this.unitId;
    }

    public  void setUnitId(@Nullable String var1) {
        this.unitId = var1;
    }

    @Nullable
    public String getWaterFallVMAP() {
        return waterFallVMAP;
    }

    public void setWaterFallVMAP(@Nullable String waterFallVMAP) {
        this.waterFallVMAP = waterFallVMAP;
    }

    @Nullable
    public  List<WaterfallTag> getWaterfallTags() {
        return this.waterfallTags;
    }


    public  void setWaterfallTags(@Nullable List<WaterfallTag> waterfallTags) {
        this.waterfallTags = waterfallTags;
    }

    @Override @NonNull
    public String toString() {
        return "AdResponse{" +
                "mobile=" + mobile +
                ", bottomMargin='" + bottomMargin + '\'' +
                ", parentDiv='" + parentDiv + '\'' +
                ", cancelTimeout='" + cancelTimeout + '\'' +
                ", id='" + id + '\'' +
                ", isMuted=" + isMuted +
                ", isPlaysInline=" + isPlaysInline +
                ", isAutoplay=" + isAutoplay +
                ", isPreload=" + isPreload +
                ", oldParentDiv='" + oldParentDiv + '\'' +
                ", adbreakOffsets=" + adbreakOffsets +
                ", showOn='" + showOn + '\'' +
                ", iframeBurst='" + iframeBurst + '\'' +
                ", sources=" + sources +
                ", path='" + path + '\'' +
                ", unitId='" + unitId + '\'' +
                ", waterFallVMAP='" + waterFallVMAP + '\'' +
                ", waterfallTags=" + waterfallTags +
                '}';
    }
}
